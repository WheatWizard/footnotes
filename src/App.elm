import Css exposing ( textAlign, paddingTop, paddingBottom, paddingLeft, paddingRight, px, center, fontFamilies )
import Browser exposing ( sandbox )
import Html.Styled exposing ( text, div, Html, button, span, sup, toUnstyled )
import Html.Styled.Events exposing ( onClick )
import Html.Styled.Attributes exposing (css, href, src)
import List exposing ( map, concat )
import String exposing ( fromInt )

concatMap : (a -> List b) -> List a -> List b
concatMap f l = map f l |> concat

type Page = Note Int (List Page) | Contents String

story : List Page
story = [
  Contents "It was a dark",
  Note 1 [
    Contents " (Well not too dark)"],
  Contents " and stormy",
  Note 2 [
    Contents " (Not to stormy either)"],
  Contents" night",
  Note 3 [
    Contents " (It was more of a twilight",
    Note 4 [
      Contents " (Perhaps why it wasn't so dark",
      Note 5 [
       Contents " (But it would get dark",
       Note 6 [
         Contents " (and it would get stormy)"],
       Contents ")"],
      Contents ")"],
    Contents ")"],
  Contents "."]

expandNote : Int -> Page -> List Page
expandNote n page = case page of
  Contents a -> [page]
  Note x pages ->
    if x == n then
      pages
    else
      [Note x pages]

-- Model

type alias Model = List Page

init : ( Model, Cmd Msg )
init = ( story, Cmd.none )

-- Messages

type Msg = Expand Int

--View 

view : ( Model, Cmd Msg ) -> Html Msg
view ( model, _ ) = decode model |> div [css [
--  textAlign center,
  paddingTop (px 200),
  paddingBottom (px 200),
  paddingLeft (px 20),
  paddingRight (px 20),
  fontFamilies [ "Verdana", "Arial" ]]]

decode : Model -> List (Html Msg)
decode model = case model of
  [] -> []
  (Contents s :: rest) -> text s :: decode rest
  (Note n _ :: rest) -> sup [ onClick (Expand n) ] [ n |> fromInt |> text ]  :: decode rest

-- Update

pure : Page -> List Page
pure x = case x of
  Contents _ -> [x]
  Note _ a -> [Contents "Hi? "]

update : Msg -> (Model, Cmd Msg) -> ( Model, Cmd Msg )
update message (model, msg) = case message of
  Expand n -> ( concatMap (expandNote n) model, msg )

-- Main

main = sandbox {
  init = init,
  update = update,
  view = toUnstyled << view}
